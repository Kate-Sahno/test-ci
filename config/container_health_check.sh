#!/bin/bash

# AWS variable
CLUSTER_NAME=$1
TASK_ARN=$(aws ecs list-tasks --cluster $CLUSTER_NAME | jq '.taskArns[0]')
# Local bash variable
health_check=1
check_index=0
checks_number=20
echo "Starting container health check..."
while [ $health_check -eq 1 ]
do
  if [ $check_index -le  $checks_number ]
  then 
    command_check_container_status="aws ecs describe-tasks --task $TASK_ARN --cluster $CLUSTER_NAME | jq '.tasks[0].containers[0].healthStatus'"
    health_status=$(eval $command_check_container_status) 
    if [[ $health_status == "\"HEALTHY\"" ]];
    then 
      health_check=0 
      echo "Container is ready. Health status: $health_status"
      exit 0
     else
      health_check=1
      ((check_index++))
      echo "Container is not ready.  HealthStatus: $health_status, check number: $check_index/$checks_number"
      sleep 10 
    fi
  else 
    exit 1
  fi
done
