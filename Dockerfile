FROM gradle:jdk12 as builder
COPY --chown=gradle:gradle . /home/gradle/project
WORKDIR /home/gradle/project
RUN gradle clean bootJar

FROM azul/zulu-openjdk-alpine:12 as app
RUN addgroup project-group -g 1001 && \
    adduser -D -u 1001 -G project-group project-user && \
    apk add --update curl && \
    rm -rf /var/cache/apk/*
COPY --from=builder --chown=project-user:project-group /home/gradle/project/build/libs/*.jar app.jar
USER project-user
EXPOSE 8080
ENTRYPOINT ["java", \
            "-XX:+UseContainerSupport", \
            "-XX:InitialRAMPercentage=50", \
            "-XX:MaxRAMPercentage=80", \
            "-Djava.security.egd=file:/dev/./urandom", \
            "-XX:+ParallelRefProcEnabled", \
            "-XX:+UseCompressedClassPointers", \
            "-jar", \
            "app.jar"]