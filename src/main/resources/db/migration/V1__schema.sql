create schema if not exists kate_project_db collate utf8_general_ci;
use kate_project_db;

create table users
(
    id int auto_increment,
    email varchar(50) not null,
    firstname varchar(20) null,
    lastname varchar(20) null,
    male tinyint(1) null,
    constraint email_UNIQUE
        unique (email),
    constraint id_UNIQUE
        unique (id)
);

alter table users
    add primary key (id);

