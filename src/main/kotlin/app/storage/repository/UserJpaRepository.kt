package app.storage.repository

import app.storage.entity.UserEntity
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional

@Repository
@Transactional(propagation = Propagation.REQUIRED, readOnly = true)
interface UserJpaRepository : JpaRepository<UserEntity?, Long?>
