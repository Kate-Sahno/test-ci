package app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.scheduling.annotation.EnableAsync

@SpringBootApplication
@EnableAsync
class App

fun main(args: Array<String>) {
    SpringApplicationBuilder().sources(App::class.java).run(*args)
}
