package app.api.controller

import app.storage.repository.UserJpaRepository
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping

@Controller
class WelcomeController(val repository: UserJpaRepository) {
    
    @GetMapping("/")
    fun main(model: Model): String {
        model.addAttribute("users", repository.findAll())
        return "welcome"
    }
}
