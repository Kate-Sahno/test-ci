package app

import app.storage.entity.UserEntity
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test

class UnitTest {
    
    @Test
    fun `test user equality`() {
        val user1 = UserEntity().apply {
            email = "test1@test.test"
            firstname = "Elon"
            lastname = "Musk"
            male = true
        }
        
        val user2 = UserEntity().apply {
            email = "test2@test.test"
            firstname = "Elon"
            lastname = "Mask"
            male = true
        }
        
        assertFalse(user1 == user2)
    }
}
